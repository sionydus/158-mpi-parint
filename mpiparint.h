#ifndef MPI_PAR_INT
#define MPI_PAR_INT

typedef struct startRange {
    int first;
    int last;
} StartRange;

typedef map<int,int> cmap;

void splitIndices(int nstarts);
void min_map(std::map<int,int> &min_map, int& min_first, int &min_last, int nx, int nstarts);
void parint ( int *x, int nx, int *starts, int nstarts, int **rslt, int *nrslt );
void combine_results(map<int,int> &freq_map, int nsets, int **res, int *nres);

#endif
