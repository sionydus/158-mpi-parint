#include <mpi.h>
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <sstream>
#include <iterator>
#include <vector>
#include <map>
#include <set>
#include "mpiparint.h"

#define PIPE_MSG 0
#define END_MSG 1

#define PRINT_DATA(data, n)						\
    cout << "Hi, I'm node " << me << endl;				\
    cout << "My range is " << my_first << "-" << my_last << endl;	\
    do {								\
        printf("{");							\
        for (int i = 0; i < n-1; i++)					\
            printf("%d, ", data[i]);					\
        printf("%d}\n", data[n-1]);					\
    } while(0)

using namespace std;

// FOR DEBUGGING
int nnodes, n, me;
int *glob, *glob_starts;
double t1, t2;
std::map<int,int> freq_map;
int *range;


void splitIndices(int nstarts)
{
    int **indices = new int*[nnodes];

    int chunkSize = nstarts / nnodes;
    if(nstarts % nnodes != 0)
        chunkSize ++;
    // delta is the number of nodes having one less task
    int delta = chunkSize * nnodes - nstarts;

    // TODO could optimize the arithmetic expression
    for ( int i = 0; i < nnodes ; i++) {
        indices[i] = new int[2];
        if(i >= delta){
            indices[i][0] = delta * (chunkSize - 1) + (i - delta) * chunkSize;
            indices[i][1] = delta * (chunkSize - 1) + (i - delta + 1) * chunkSize - 1;
        } else {
            indices[i][0] = i * (chunkSize - 1);
            indices[i][1] = (i + 1) * (chunkSize - 1) - 1;
        }
    }

    for (int i = 0; i < nnodes; i++)
	// note that range records indices of starts, not x
        MPI_Send(indices[i], 2, MPI_INT, i, 0, MPI_COMM_WORLD);

    delete [] indices;
}
// don't forget to make glob_starts global
void min_map(std::map<int,int> &min_map, int& min_first, int &min_last, int nx, int nstarts)
{
    int min_size;
    int my_first = range[0];
    int my_last = range[1];

    //check for case with only one set
    if( my_first == my_last && my_last == nstarts - 1) {
	min_first = glob_starts[my_first];
	min_last  = nx; //min_last gets last position of smallest set
    }
    else {
	min_first = glob_starts[my_first];
	min_last  = glob_starts[my_first+1];
	min_size = glob_starts[my_first+1] - glob_starts[my_first];
    }

    //finds first and last index of the smallest set
    for (int i = my_first+1; i <= my_last+1; i++) {

        int last_index;
	int first_index = glob_starts[i-1];

	//check for end cases when we get the last index of the last set
 	i == nstarts ? last_index = nx : last_index = glob_starts[i];

        if ((last_index - first_index) < min_size) {
            min_first = first_index;
            min_last  = last_index;
            min_size = last_index - first_index;
        }

    }
    //stores intersects into the hash table
    for (int i = min_first; i < min_last; min_map[glob[i++]] = 1);

}

// manager code
void combine_results(map<int,int> &freq_map, int nsets, int **res, int *nres)
{
    int* results;
    int nresults;
    MPI_Status status;
    //we want a fresh new map
    freq_map.clear();
    // receive intersect sets and form overall intersect
    int num_of_intersects = 0;
    for (int i = 0; i < nnodes; i++) {
        MPI_Recv(&nresults, 1, MPI_INT, i, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
	//allocation of memory necessary to receive the array of intersects
	//printf("node = %d, got this %d\n",me, nresults);
	if(nresults > 0) {
	    num_of_intersects++;
	    results = new int[nresults];
	    MPI_Recv(results, nresults, MPI_INT, i, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
	    for (int j = 0; j < nresults; j++) {
		freq_map[results[j]]++;
	    }
	}
    }

//    *nres = freq_map.size();
    *res = new int[freq_map.size()];
    int i = 0;
    for (map<int,int>::iterator it=freq_map.begin(); it != freq_map.end(); ++it) {
	if(it->second == num_of_intersects) {
	    (*res)[i++] = it->first;
	}
    }
    *nres = i;

    //used for timing
//    t2 = MPI::Wtime();

//    printf("elapsed time = %f\n", (float)(t2-t1));
    // PRINT_DATA(intersect.keys(), intersect.size());
}

void parint(int *x, int nx, int *starts, int nstarts,
            int **rslt, int *nrslt)
{
    int i;
    int min_first, min_last;
    vector<int> intersect;
    int set_size;
    map<int,int> freq_map;
    MPI_Status status;

    glob = x;
    glob_starts = starts;

    //get node number
    MPI_Comm_rank(MPI_COMM_WORLD, &me);
    //get number of nodes
    MPI_Comm_size(MPI_COMM_WORLD, &nnodes);

    // [0] contains first index of set
    // [1] contains first index of last set
    range = new int[2];

//    if (me == nnodes-1) t1 = MPI::Wtime();
    //node 0 will start splitting indices and
    //sends respective first and last values to all nodes
    if (me == 0) splitIndices(nstarts);

    MPI_Recv(range, 2, MPI_INT, 0, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
    MPI_Bcast(&nstarts, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(starts, nstarts, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&nx, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(x, nx, MPI_INT, 0, MPI_COMM_WORLD);

//    printf("node %d has first = %d, last = %d\n", me, range[0], range[1]);

    //check for last == -1 to prevent lots of calculations
    if(range[1] != -1) {

	int my_first = starts[range[0]];

	//last position of the chunk
	int my_last = starts[range[1]];
	if(range[1] == nstarts - 1) {
	    my_last = nx; //takes care of last node
	}
	else { //gets the last position of last set in chunk
	    my_last = starts[range[1] + 1];
	}

	// each thread finds intersection of min with their respective chunk
	min_map(freq_map, min_first, min_last, nx, nstarts);

	// checks every set for intersects (skips the min set)
	// increment when there are intersects
	for (i = my_first; i < my_last; i++) {
	    if (! (min_first <= i && i < min_last)) {
		if (freq_map.find(glob[i]) != freq_map.end()){
		    freq_map[glob[i]]++;
		}
	    }
	}

	set_size = min_last - min_first;
	int num_sets = range[1] - range[0] + 1;

	for (map<int,int>::iterator it=freq_map.begin(); it != freq_map.end(); ++it)    {
	    if (it->second == num_sets) {
		intersect.push_back(it->first);
	    }
	}
    }//end if

//debugging
//    PRINT_DATA(intersect, intersect.size());
    delete [] range;

    int nintersect = intersect.size();

    // send number of intersects found to node 0
    MPI_Send(&nintersect, 1, MPI_INT, 0, PIPE_MSG, MPI_COMM_WORLD);
    // send the intersects found to node 0
    if(nintersect > 0) {
	MPI_Send(&intersect[0], intersect.size(), MPI_INT, 0, PIPE_MSG, MPI_COMM_WORLD);
    }
    // should be barrier here
    MPI_Barrier(MPI_COMM_WORLD);
    //node 0, go combine the results
    if (me == 0) combine_results(freq_map, nstarts, rslt, nrslt);

}

// int main(int argc, char **argv)
// {


//     MPI::Init();


//     int nrslt;
//     int *rslt;

//     // //0               7             13
//     int x[74] = {1,2,3,4,5,6,7,  1,2,3,4,5,6,  1,2,3,4,5,
//     			//18              25            31
//     			9,2,3,0,5,9,7,  1,2,3,4,5,6,  1,2,3,4,5,
//     			//36              43            49
//     			1,2,3,4,5,6,7,  1,2,3,4,5,6,  1,2,3,4,5,
//     			//54       58        62      65          70
//     			1,2,3,4,  1,2,3,4,  1,2,3,  9,1,2,3,4,  1,2,3,4};

//     int starts[14] = {0,7,13,18,25,31,36,43,49,54,58,62,65,70};

//     int nxx = 74;
//     int nstartsx = 14;


//     // static int x[1] = {1};

//     // static int starts[1] = {0};

//     // int nxx = 1;
//     // int nstartsx = 1;

//     int nx = nxx;
//     int nstarts = nstartsx;


//     parint(x, nx, starts, nstarts, &rslt, &nrslt);



//     // Matt will call
//     MPI::Finalize();

//     // for(int i = 0; i < nrslt; i++) {
//     // 	printf("%d ", rslt[i]);
//     // }
//     // printf("\n");

//     return 0;
// }
